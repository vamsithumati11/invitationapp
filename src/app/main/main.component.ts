import { Component, DefaultIterableDiffer, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import data from './data';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit {
  params: any;
  data = data.data;
  eventData = data.eventData;
  imageName = 'default.jpg';
  content;
  name: any;
  toName;
  saveTheDate;
  venue;
  eventDate;
  cObject: any;
  locationURL: any;
  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.params.subscribe((params) => {
      this.params = params;
      if (this.params.id && this.eventData[this.params.id] && params.to) {
        this.saveTheDate = this.eventData[this.params.id].saveTheDate;
        this.toName = this.eventData[this.params.id].toName;
        this.eventDate = this.eventData[this.params.id].eventDate;
        this.venue = this.eventData[this.params.id].venue;
        this.locationURL = this.eventData[this.params.id].locationURL;
      } else {
        this.router.navigate(['/']);
      }
      this.cObject = this.data.some((f) => {
        return f.Name.toLowerCase().trim() === params.to.toLowerCase().trim();
      })
        ? this.data.filter((f) => {
            return (
              f.Name.toLowerCase().trim() === params.to.toLowerCase().trim()
            );
          })[0]
        : {displayName: 'Human' , Name: 'Human', Image: 'Default.jpg', Content: ''};
    });
  }

  ngOnInit(): void {}
}
